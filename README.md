# Docker Course Basics

## Requirements

- git
- docker / docker-compose

## Installation

- Create a new file `.env` in the root directory, copy all contents from `.env.example` into it, and edit as you wish.
- Run `make build` to build the images
- Run `make up` to start the docker containers

## How does it work

There are two separate containers. 
- app: this container contains all data from the `app` folder
- webserver: this container is the nginx webserver

### app container

The actual app is inside the `app` folder. When build, it will pass the local user uid and gid to the container. This 
helps with permission problems (for example with laravel storage and logs folder).

### webserver container

When building this container, it will get the `WEBSERVER_PORT` variable from the `.env` file and expose the
internal port `80` to the specified port.

It also will mount the `nginx.conf` from the dockerfile folder into the nginx config folder inside the container.

We also mount everything from the `app/public` folder to the `var/www` folder, to define an entrypoint. 

## Useful commands

- `make build`: builds the images
- `make up`: starts the containers
- `make daemon`: starts the container in daemon mode
- `make stop`: stops all containers 
- `make down`: removes all containers 
- `make bash`: creates a bash inside the app container
- `make bash-webserver`: creates a bash inside the webserver container

## Author & Contact

This project was created by Thilo Hettmer.
Gitlab: https://gitlab.com/DerThilo

## License

MIT License

Copyright (c) 2020 Thilo Hettmer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.