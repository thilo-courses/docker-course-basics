build:
	UID=${shell id -u} GID=${shell id -g} docker-compose build
up:
	docker-compose up
daemon:
	docker-compose up -d
stop:
	docker-compose stop
down:
	docker-compose down
bash:
	docker-compose exec app bash
bash-web:
	docker-compose exec webserver bash